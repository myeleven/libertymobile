check_auth();

var page = $('.page').attr('data-page-name');

if(page === 'cart-page') {

    drawCartPage();

}

if(page === 'order-page') {
    var userCart = localStorage['cart'];

    if(userCart === null) {
        window.location.href='index.html';
    }

    $('input[name=delivery_type]').on('change', function() {

        $('.receiver-data').toggleClass('d-none');

        $('.customer-data').toggleClass('d-none');

    });

    $('.order-form').on('submit', function(e) {

        e.preventDefault();

        var allFormData = $(this).serializeJSON(), userData = localStorage.getItem('userAuth');


        allFormData['goods'] = JSON.parse(localStorage['cart']);

        allFormData['mobile'] = 1;

        allFormData['user_id'] = JSON.parse(userData)['id'];

        console.log(allFormData);

        $.ajax({
            url: 'http://libertyflowers.ru/api.php',
            method: 'POST',
            dataType: 'JSON',
            data: {make_order: allFormData},
            success: function (response) {

                console.log(response);
                if(response.ok !== undefined) {
                    window.location.href = 'thanks.html';
                    delete localStorage['cart'];
                }
            }
        });

    });

}

$(document).ready(function () {



    $('.user-auth-form').submit(auth_user);

    $('.logout-btn').on('click', clear_storage);


    if(page === 'detail-page') {

        $('#addItemCount').on('click', addItemCount);

        $('#minusItemCount').on('click', minusItemCount);

        $('#add-to-cart').on('click', addToCart);

    }

    if(page === 'cart-page') {
        setTimeout(function (e) {
            var items = $('.wrap-cart-item'),
                items_length = items.length,
                resultCost = 0;

            console.log("items", items);

            for(var i = 0; i < items_length; i++) {

                console.log("cost", +$(items[i]).find('#item-cost').html());
                console.log("count", +$(items[i]).find('#item-count').html());
                resultCost += +$(items[i]).find('#item-cost').html() * +$(items[i]).find('#item-count').html()
            }

            console.log(resultCost);

            $('#resultCost').html(resultCost);
        },1000);
    }

});

function check_auth() {

    var response_string = localStorage.getItem('userAuth'),
        response = JSON.parse(response_string),
        page = $('.page').attr('data-page-name');

        console.log(response);

        if(response === null || response.status === false) {
            $('.logout-btn').hide();
            $('.cabinet-btn').hide();

            if(page === 'cabinet-page' || page === 'order-page') {
                window.location.href = 'login.html';
            }
        }

        if(response !== null && response.status === true ) {


            $('.none-auth-btn').hide();

            if(page === 'cabinet-page') {
                get_user_data();
            }


            if(page === 'login-page' || page === 'register-page') {
                window.location.href = 'index.html';
            }

        }

        if(page === 'catalog-page') {
            get_catalog();
        }

        if(page === 'detail-page') {
            get_item_by_id();
        }
}

function clear_storage() {
    delete localStorage['userAuth'];

    window.location.href = '/';
}

function auth_user() {


    var authForm = $('.user-auth-form'),
        email = authForm.find('#user_email').val(),
        password = authForm.find('#user_pass').val();

    if(email === '') {
        alert('Введите email!');
        return false;
    }

    if(password === '') {
        alert('Введите пароль!');
        return false;
    }

    var obj = {
      'email':email,
      'password':password
    };



    $.ajax({
        url: 'http://libertyflowers.ru/api.php',
        data: {auth_user : obj},
        dataType: 'JSON',
        method: 'POST',
        success: function (response) {


            if(response.ok !== undefined) {

                response['status'] = true;

                console.log(response);

                localStorage['userAuth'] = JSON.stringify(response);

                window.location.href = 'index.html';
            }
        }
    });

    return false;
}

function get_catalog() {
    $.ajax({
        url: 'http://libertyflowers.ru/api.php',
        data: {getCatalog : ''},
        dataType: 'JSON',
        method: 'POST',
        success: function (response) {

            console.log(response);

            $.each(response.rows, function (i, row) {
                $('.items-list .row').append(
                    '              <div class="col-6 mt-3">' +
                    '                <a href="detail.html?id=' + row.id + '">' +
                    '                  <div class="card">' +
                    '                    <img class="card-img-top" src="http://libertyflowers.ru' + row.image[0].url_path + '" alt="Card image cap">' +
                    '                    <div class="card-body">' +
                    '                      <h5 class="card-title m-0">' + row.name + '</h5>' +
                    '                      <h5 class="card-title m-0">от ' + row.cost + ' р. </h5>' +
                    '                    </div>' +
                    '                  </div>' +
                    '                </a>' +
                    '              </div>');
            })

        }
    });

    return false;
}

function get_user_data() {

    var response_string = localStorage.getItem('userAuth'),
        response = JSON.parse(response_string),
        userId = response.id;

    $.ajax({
        url: 'http://libertyflowers.ru/api.php',
        data: { getUserData : { user_id: userId } },
        dataType: 'JSON',
        method: 'POST',
        success: function (response) {
            console.log(response);
        }
    })

}

function get_item_by_id(itemId = null) {
    var id = gup('id');

    if(itemId !== null) {
        id = itemId;
    }

    $.ajax({
        url: 'http://libertyflowers.ru/api.php',
        data: { getItem : { id: id } },
        dataType: 'JSON',
        method: 'POST',
        success: function (response) {
            $('#item-image').attr('src',"http://libertyflowers.ru" + response.image[0]['url_path']);
            $('#item-title').html(response.name);
            $('#item-cost').html(response.cost + " p.");
            $('#item-descr').html(response.description);
            $('#item-composition').html(response.composition);
            $('#add-to-cart').attr('data-item-id', response.id);
        }
    });
}

function gup( name, url ) {
    if (!url) url = location.href;
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( url );
    return results == null ? null : results[1];
}

function addItemCount() {
    $('#itemsCount').val( +$('#itemsCount').val() + 1 );
}

function minusItemCount() {
    var items = $('#itemsCount').val();

    if(items > 1) {
        $('#itemsCount').val( +$('#itemsCount').val() - 1 );
    }
}

function addToCart() {
    var itemId = $('#add-to-cart').attr('data-item-id'),
        countItems = $('#itemsCount').val();

    var cart_json = localStorage.getItem('cart');

    if(cart_json === null) {

        var obj = {};

        obj[itemId] = countItems;

        localStorage['cart'] = JSON.stringify(obj);
    } else {

        var cart_obj = JSON.parse(cart_json);

        cart_obj[itemId] = countItems;

        localStorage['cart'] = JSON.stringify(cart_obj);
    }
}

function drawCartPage() {

    var cart_json = localStorage.getItem('cart');

    if(cart_json === null) {
        $('.cart-items').html('В Вашей корзине нет товаров!');

        return false;
    }


    var cart_obj = JSON.parse(cart_json);

    $.each(cart_obj, function (i, row) {

        $.ajax({
            url: 'http://libertyflowers.ru/api.php',
            data: { getItem : { id: i } },
            dataType: 'JSON',
            method: 'POST',
            success: function (response) {

                var itemsList = $('.cart-items');

                itemsList.append('<a href="detail.html?id=' + response.id + '" class="wrap-cart-item"><div class="row">' +
                    '                                <div class="col-3">' +
                    '                                    <div class="wrap-item-image">' +
                    '                                        <img src="http://libertyflowers.ru' + response.image[0].url_path + '" alt="image">' +
                    '                                    </div>' +
                    '                                </div>' +
                    '                                <div class="col-9">' +
                    '                                    <div class="wrap-cart-item-description">' +
                    '                                        <h4 class="p-0 m-1">' + response.name + '</h4>' +
                    '                                        <p class="p-0 m-1"><span id="item-cost">' + response.cost + '</span> р.</p>' +
                    '                                        <p class="p-0 m-1">Количество: <span id="item-count">' + row + '</span></p>' +
                    '                                    </div>' +
                    '                                </div>' +
                    '                            </div></a>');

            }
        });


    });


}